var express = require('express');
var jsdom = require('jsdom');
var request = require('request');
var url = require('url');
var router = express.Router();

router.get('/', function(req, res, next) {
	//Tell the request that we want to fetch youtube.com, send the results to a callback function
	request({uri: 'http://youtube.com'}, function(err, response, body){
		var self = this;
		self.items = new Array();//I feel like I want to save my results in an array
		//Just a basic error check
		console.log(response.statusCode);
		if(err && response.statusCode !== 200){console.log('Request error.');}
		//Send the body param as the HTML code we will parse in jsdom
		//also tell jsdom to attach jQuery in the scripts and loaded from jQuery.com
		jsdom.env({
			html: body,
			scripts: ['http://code.jquery.com/jquery-1.6.min.js']
		}, function(err, window){
			//Use jQuery just as in a regular HTML page
			var $ = window.jQuery;
			console.log($('title').text());
			res.end($('title').text());
		});
	});
});

module.exports = router;